<?php

class SellerOrdersController extends \BaseController {

	/**
	 * Display a listing of orders
	 *
	 * @return Response
	 */
	public function index()
	{
		$orders = Order::all();
		return View::make('seller.orders.index', compact('orders'));
	}

	/**
	 * Display the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::find($id);
		return View::make('seller.orders.show', compact('order'));
	}

	//show list of orders for specific user
	public function listing(){
		//echo $user;
		$hostparts = explode('.',$_SERVER['HTTP_HOST']);
		$username = $hostparts[0];
		$user = User::where('username','=',$username)->first();
		
		//if ($user == Auth::user()) {
			
			$orders = Order::where('user_id','=',$user->id)->get();
			$totalPenjualanUserHariIni = Order::date()->user()->get();

			return View::make('seller.orders.index',compact(array('user','orders','totalPenjualanUserHariIni')));
		//}
		//else{
		//	echo "No restricted access";
		//}
		
	}

	/**
	 * Show the form for editing the specified order.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::find($id);

		return View::make('seller.orders.edit', compact('order'));
	}

	/**
	 * Update the specified order in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$order = Order::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Order::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$order->update($data);

		return Redirect::route('seller.orders.index');
	}

	/**
	 * Remove the specified order from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Order::destroy($id);

		return Redirect::route('seller.orders.index');
	}

}

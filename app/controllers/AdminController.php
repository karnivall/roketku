<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function showUsers()
	{
		$users = User::where('role','=','Seller')->get();
		return View::make('admin.users.show', compact('users'));
		//echo $users->count();
		/*
		for($i=0;$i<$users->count();$i++){
			$user = $users[$i];
			$user_id = $user->id;
			
			$orders = Order::where('user_id','=',$user_id)->get();
			
			//for($j=0;$j<$orders->count();$j++){
			//	$order = $orders[$j];
			//}
		}*/
	}

	public function editUser($id){
		$user = User::where('id','=',$id)->first();
		return View::make('admin.users.edit',compact('user'));
	}

	public function doEditUser($id){
		$user = User::findOrFail($id);

		$rules = array(
				'username' => 'required|max:50',
				'email' => 'required|max:50|email',
				'online_shop_name' => 'required',
				'name' => 'required',
				'phoneAtauLine' => 'required',
				'alamat'=>'required'
		);
		$valid = Validator::make($data = Input::all(),$rules);

		if($valid->fails()) {
			return Redirect::back()->withErrors($valid)->withInput();
		}

		$user->update($data);

		return Redirect::route('get.admin.showList');

		if($user) {
			/*
			$email = Mail::send('emails.activate', array('name' => $name, 'link' => URL::route('activate-account', $code)), function($message) use ($user) {
				$message->to($user->email, $user->name)->subject('Activate your account');
			});

			if($email) {
				return Redirect::route('sign-up')->with('success', true);
			}
			*/

			return View::make('order.success');
		}

		return Redirect::route('get.admin.editUser')->with('unex-error', true)->withInput();
	}

}

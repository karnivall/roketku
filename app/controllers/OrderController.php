<?php

class OrderController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('order.index');
	}

	public function order($username){
		$user = User::where('username','=',$username)->first();
		return View::make('order.specific',compact('user'));
	}

	public function orderSpecific(){
		$hostparts = explode('.',$_SERVER['HTTP_HOST']);
		$username = $hostparts[0];
		$user = User::where('username','=',$username)->first();

		if ($user->role === 'Seller') {
			return View::make('order.specific',compact('user'));
		}
		else{
			return Redirect::route('get.login.showLogin');
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			array(
				'email' => 'required|email',
				'nama' => 'required',
				'alamat' => 'required',
				'kodePos' => 'required',
				'jenisBarang' => 'required',
				'phoneAtauLine' => 'required',
				'imageBuktiPembayaran' => 'required',
				'totalHarga' => 'required',
				'totalBerat' => 'required',
				'biayaOngkir' => 'required'
			)
		);

		if($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else{
			$hostparts = explode('.',$_SERVER['HTTP_HOST']);
			$username = $hostparts[0];

			$image = Input::file('imageBuktiPembayaran');
			$destinationPath = 'uploads'.'/'.$username;
			$filename = $image->getClientOriginalName();

			$date = Date::now()->format('dWmYHisu');
			$filenameFinal = $date.'-'.$filename;
			Input::file('imageBuktiPembayaran')->move($destinationPath, $filenameFinal);

			$order = Order::create(['user_id'       => Input::get('user_id'),
							'email'       => Input::get('email'),
                           'nama'       => Input::get('nama'),
                           'alamat' => Input::get('alamat'),
                           'kodePos' => Input::get('kodePos'),
                           'jenisBarang'      => Input::get('jenisBarang'),
                           'totalBerat' => Input::get('totalBerat'),
                           'phoneAtauLine'      => Input::get('phoneAtauLine'),
                           'imageBuktiPembayaran'        => $destinationPath .'/'. $filenameFinal,
                           'totalHarga'      => Input::get('totalHarga'),
                           'biayaOngkir'      => Input::get('biayaOngkir'),
                           ]);
			
			if ($order) {
		        return View::make('order.success');
		    }
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

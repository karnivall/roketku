<?php

class SettingController extends \BaseController {

	public function showSetting(){
		$user = Auth::user();
		return View::make('setting',compact(array('user')));
	}

	public function doEditUser(){
		$user = Auth::user();

		if ($user->role === 'Admin') {
			$rules = array(
				'email' => 'required|max:50|email',
				'name' => 'required',
				'phoneAtauLine' => 'required',
				'alamat'=>'required'
			);
		}else{
			$rules = array(
					'email' => 'required|max:50|email',
					'online_shop_name' => 'required',
					'name' => 'required',
					'phoneAtauLine' => 'required',
					'alamat'=>'required'
			);
		}

		$valid = Validator::make($data = Input::all(),$rules);

		if($valid->fails()) {
			return Redirect::back()->withErrors($valid)->withInput();
		}

		$user->update($data);

		if ($user->role === 'Admin') {
			return Redirect::route('get.admin.showList');
		}
		else{
			return Redirect::route('seller.order.listing');
		}
		
	}

	public function doEditPassword(){
		$currentPassword = Input::get('current_password');


		$user = Auth::user();

		$rules = array(
			'current_password' => 'required',
			'new_password' => 'required',
			'password_again' => 'required|same:new_password'
		);

		$valid = Validator::make(Input::all(),$rules);

		if($valid->fails()) {
			return Redirect::back()->withErrors($valid)->withInput();
		}

		if (Hash::check($currentPassword, $user->password)) {
			$newPassword = Input::get('new_password');
			$user->update(array(
				'password' => Hash::make($newPassword)
			));

			return Redirect::route('seller.order.listing');
		}

		return "Password tidak sama dengan yang sekarang";
	}

}
<?php

class LoginController extends BaseController {
	public function showLogin(){
		if(!Auth::check()){
			return View::make('login.index');
		}
		else{
			$user = Auth::user();
			if ($user->role === 'Admin') {
				return Redirect::route('get.admin.showList');
			}
			else{
				return Redirect::route('seller.order.listing');
			}
		}
	}

	public function doLogin(){
		// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::route('get.login.showLogin')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		}
		else{
			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			// attempt to do the login
			if (Auth::attempt($userdata)) {
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				$userFromLogin = Auth::user();

				$hostparts = explode('.',$_SERVER['HTTP_HOST']);
				$username = $hostparts[0];
				$userFromSubdomain = User::where('username','=',$username)->first();
				if ($userFromSubdomain == $userFromLogin) {
					if($userFromLogin->role === 'Admin'){
						return Redirect::route('get.admin.showList');
					}
					else{
						return Redirect::route('seller.order.listing');
					}
				}
				else{
					return Redirect::route('get.login.showLogin');
				}

				
			} else {	 	
				// validation not successful, send back to form	
				return Redirect::route('get.login.showLogin');
			}
		}
	}

	public function doLogout(){
		Auth::logout(); // log the user out of our application
		return Redirect::route('get.login.showLogin'); // redirect the user to the login screen
	}

	public function showRegister(){
		return View::make('register.index');
	}

	public function doRegister() {
		$rules = array(
				'username' => 'required|max:50',
				'email' => 'required|max:50|email',
				'online_shop_name' => 'required',
				'name' => 'required',
				'phoneAtauLine' => 'required',
				'password' => 'required|min:5',
				'profile_picture' => 'required',
				'password_again' => 'required|same:password'
		);
		$valid = Validator::make(Input::all(),$rules);

		if($valid->fails()) {
			return Redirect::route('get.register.showRegister')->withErrors($valid)->withInput();
		}
		else{
			$username = Input::get('username');
			$email = Input::get('email');
			$online_shop_name = Input::get('online_shop_name');
			$name = Input::get('name');
			$phoneAtauLine = Input::get('phoneAtauLine');
			$password = Input::get('password');
			$role = Input::get('role');
			$statusOnline = Input::get('statusOnline');
			$alamat = Input::get('alamat');


			$profile_picture = Input::file('profile_picture');
			$filenameFinal = 'pp.'.$profile_picture->getClientOriginalExtension();
			$destinationPath = 'uploads'.'/'.$username ;
			Input::file('profile_picture')->move($destinationPath, $filenameFinal);

			$user = User::create(array(
				'username' => $username,
				'email' => $email,
				'online_shop_name' => $online_shop_name,
				'name' => $name,
				'phoneAtauLine' => $phoneAtauLine,
				'password' => Hash::make($password),
				'profile_picture' => $destinationPath .'/'. $filenameFinal,
				'role' => $role,
				'statusOnline' => $statusOnline,
				'alamat' => $alamat
			));
		}

		if($user) {
			/*
			$email = Mail::send('emails.activate', array('name' => $name, 'link' => URL::route('activate-account', $code)), function($message) use ($user) {
				$message->to($user->email, $user->name)->subject('Activate your account');
			});

			if($email) {
				return Redirect::route('sign-up')->with('success', true);
			}
			*/

			return View::make('order.success');

		}

		return Redirect::route('get.register.showRegister')->with('unex-error', true)->withInput();
	}

}
<?php

class Order extends \Eloquent {
	protected $fillable = [
		'user_id',
		'jenisBarang',
		'phoneAtauLine',
		'imageBuktiPembayaran',
		'totalHarga',
		'biayaOngkir',
		'confirmed',
		'email',
		'nama',
		'alamat',
		'kodePos',
		'totalBerat'
	];

	public function user(){
		return $this->belongsTo('User');
	}

	public function scopeDate($query){
		return $query->where('created_at','>=',new DateTime('today'));
	}

	public function scopeUser($query){
		$user = Auth::user();
		return $query->where('user_id','=',$user->id);
	}
	
}
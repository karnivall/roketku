<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->text('jenisBarang');
			$table->text('email');
			$table->text('nama');
			$table->text('alamat');
			$table->text('kodePos');
			$table->text('phoneAtauLine');
			$table->string('imageBuktiPembayaran');
			$table->string('totalHarga');
			$table->string('totalBerat');
			$table->string('biayaOngkir');
			$table->boolean('confirmed');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}

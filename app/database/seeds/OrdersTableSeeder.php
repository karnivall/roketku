<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Order::create([
				'user_id' => rand(1,3),
				'jenisBarang' => $faker->randomElement($array = array ('Casing iPhone 6 ijo : 1','Makeup Set A : 1 buah','Casing iPhone 6 Green : 2 piece','Casing iPhone 6 Blue Jelly : 5 buah','Casing iPhone 5s : white dove 1buah')),
				'email' => $faker->email(),
				'nama' => $faker->name(),
				'alamat' => $faker->address(),
				'kodePos' => $faker->postcode(),
				'phoneAtauLine' => $faker->phoneNumber(),
				'imageBuktiPembayaran' => $faker->imageUrl($width = 640, $height = 480),
				'totalHarga' => $faker->randomNumber($nbDigits = NULL),
				'totalBerat' => rand(1,5),
				'biayaOngkir' => $faker->numberBetween($min = 5000, $max = 32000),
				'confirmed' => 0
			]);
		}
	}

}
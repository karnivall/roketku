<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 3) as $index)
		{
			User::create([
				'username' => $faker->userName(),
				'profile_picture' => $faker->imageUrl($width = 200, $height = 200),
				'online_shop_name' => $faker->userName . ' Online Shop',
				'name' => $faker->name(),
				'email' => $faker->email(),
				'phoneAtauLine' => $faker->phoneNumber(),
				'password' => Hash::make('12345'),
				'alamat' => $faker->address(),
				'role' => $faker->randomElement($array = array ('Admin','Seller')),
				'statusOnline' => 0
			]);
		}
	}

}
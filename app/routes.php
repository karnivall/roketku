<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::group(array('domain' => 'admin.roketku.local'), function()
// {
//     Route::get('/',function() {
//     	echo "helo";
//     });
// });

Route::resource('order','OrderController',array('only'=>array('store')));

Route::group(array('prefix'=>'admin'),function(){
	Route::get('login',array('uses'=>'LoginController@showLogin','as'=>'get.login.showLogin'));
	Route::post('login',array('uses'=>'LoginController@doLogin','as'=>'post.login.doLogin'));
});

Route::group(array('before'=>'auth','prefix'=>'admin'),function(){
	Route::get('logout', array('uses' => 'LoginController@doLogout','as'=>'get.login.doLogout'));			
});

Route::group(array('before'=>'auth'),function(){
	Route::get('admin/users/list',array('uses'=>'AdminController@showUsers','as'=>'get.admin.showList'));
	Route::get('admin/register',array('uses'=>'LoginController@showRegister','as'=>'get.register.showRegister'));
	Route::post('admin/register',array('uses'=>'LoginController@doRegister','as'=>'post.register.doRegister'));
	Route::get('admin/users/{id}/edit',array('uses'=>'AdminController@editUser','as'=>'get.admin.editUser'));
	Route::put('admin/users/{id}/edit',array('uses'=>'AdminController@doEditUser','as'=>'put.admin.editUser'));

	Route::get('orders',array('uses'=>'SellerOrdersController@listing','as'=>'seller.order.listing'));
	Route::get('setting',array('uses'=>'SettingController@showSetting','as'=>'get.setting.show'));
	Route::put('setting/edit/user',array('uses'=>'SettingController@doEditUser','as'=>'put.setting.editUser'));
	Route::put('setting/edit/password',array('uses'=>'SettingController@doEditPassword','as'=>'put.setting.changePassword'));
	//Route::get('test',array('uses'=>'SellerOrdersController@index','as'=>'seller.order.index'));
});

Route::group(array('prefix'=>'admin','before'=>'auth'), function($account){
	//Route::resource('orders', 'SellerOrdersController',array('except'=>array('create','store')));
	//Route::get('orders',array('uses'=>'SellerOrdersController@listing','as'=>'seller.order.listing'));
});

Route::get('/',array('uses'=>'OrderController@orderSpecific','as'=>'order.specific'), function($account, $id){});

//Route::group(array('domain' => '{username}.roketku.local'), $appRoutes);
//Route::group(array('domain' => 'carterdeanna.roketku.local'), $appRoutes);
//Route::group(array('domain' => 'cydney97.roketku.local'), $appRoutes);






<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <link rel="stylesheet" href="{{ URL::asset('assets/stylesheets/frontend.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/stylesheets/main_frontend.css') }}">
    <script src="{{ URL::asset('assets/javascript/frontend.js') }}"></script>
</head>
<body>

@include('partials.menu')


@yield('content')

@section('sidebar')
	<!-- <h2>Link</h2> -->
@show

</body>
</html>
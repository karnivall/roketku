

<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php
            $hostparts = explode('.',$_SERVER['HTTP_HOST']);
            $username = ucfirst($hostparts[0]);
            $title = $username . ' Online Shop';
            echo $title;
        ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <link rel="stylesheet" href="{{ URL::asset('assets/stylesheets/frontend.css') }}">
    <script src="{{ URL::asset('assets/javascript/frontend.js') }}"></script>
</head>
@if(!Auth::check())
	<body style="background-color: #ffd654">
@else
	<body style="background-color: #6ec9c8">
@endif


@include('partials.menu')


@yield('content')

@section('sidebar')
	<!-- <h2>Link</h2> -->
@show

@include('partials.footer')

</body>
</html>
@extends('layouts.default')

@section('content')

<div class="container" style="background-color:white;margin-bottom:20px;padding-bottom:20px">
    {{ Form::open(array('route'=>'post.register.doRegister','method'=>'post','role'=>'form','files'=>true)) }}
    <div class="form-group">
    	{{ Form::label('username','Username',array('class'=>'')) }}
		{{ Form::text('username', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
		<span style="color:red">{{ $errors->first('username') }}</span>
	</div>
    <div class="form-group">
        {{ Form::label('email','Email Aktif',array('class'=>'')) }}
        {{ Form::email('email', null, array('class'=>'form-control','placeholder'=>'Email yang aktif')) }}
        <span style="color:red">{{ $errors->first('email') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('online_shop_name','Online Shop Name',array('class'=>'')) }}
        {{ Form::text('online_shop_name', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
        <span style="color:red">{{ $errors->first('online_shop_name') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('name','Nama Lengkap',array('class'=>'')) }}
        {{ Form::text('name', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
        <span style="color:red">{{ $errors->first('name') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('alamat','Alamat',array('class'=>'')) }}
        {{ Form::text('alamat', null, array('class'=>'form-control','placeholder'=>'Alamat lengkap sesuai dengan alamat pengambilan')) }}
        <span style="color:red">{{ $errors->first('alamat') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('phoneAtauLine','No. Telp',array('class'=>'')) }}
        {{ Form::text('phoneAtauLine', null, array('class'=>'form-control','placeholder'=>'No. telpon / LINE ID yang dapat dihubungi')) }}
        <span style="color:red">{{ $errors->first('phoneAtauLine') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('password','Password',array('class'=>'')) }}
        {{ Form::password('password', array('class'=>'form-control','placeholder'=>'Password')) }}

        <span style="color:red">{{ $errors->first('password') }}</span>
    </div>

    <div class="form-group">
        {{ Form::label('password_again','Confirm Password',array('class'=>'')) }}
        {{ Form::password('password_again', array('class'=>'form-control','placeholder'=>'Re-Password again')) }}
        <span style="color:red">{{ $errors->first('password_again') }}</span>
    </div>

    <div class="form-group">
        {{ Form::label('profile_picture','Profile Picture',array('class'=>'')) }}
        {{ Form::file('profile_picture') }}
        <span style="color:red">{{ $errors->first('profile_picture') }}</span>
    </div>

    <div class="form-group">
        {{ Form::label('role','Role',array('class'=>'')) }}
        {{ Form::select('role', array('Seller' => 'Seller','Admin' => 'Admin'), 'Seller',array('class'=>'form-control')); }}
        <span style="color:red">{{ $errors->first('role') }}</span>
    </div>

    
        {{ Form::hidden('statusOnline', 0, array()) }}
        {{ Form::hidden('confirmed', 0, array()) }}
        {{ Form::submit('Register', array('class'=>'btn btn-primary btn-lg','style'=>'background-color:#ff743d;border-color:white')) }}
    	

    {{ Form::close() }}
</div>
@stop
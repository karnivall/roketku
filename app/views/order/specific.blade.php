@extends('layouts.order_layout')

@section('content')

<div class="container" style="background-color:white;margin-bottom:20px;padding-bottom:20px">
    <div class="header" style="margin-bottom:20px;margin-top:30px">
        <div class="row">
            <div class = "col-md-12 text-center">
                <img src="{{{$user->profile_picture}}}" class="img-circle" style="width:80px;height:80px">
            </div>
        </div>
        <div class="row">
                <div class = "col-md-12">
                    <h2 class="text-center">{{{ $user->online_shop_name }}}</h2>
                </div>
        </div>
    </div>
    {{ Form::open(array('route'=>'order.store','method'=>'post','role'=>'form','files'=>true)) }}
    {{ Form::hidden('user_id', $user->id) }}
    <div class="form-group">
        {{ Form::label('email','Email Aktif',array('class'=>'')) }}
        {{ Form::email('email', null, array('class'=>'form-control','placeholder'=>'Email yang aktif')) }}
        <span style="color:red">{{ $errors->first('email') }}</span>
    </div>
    <div class="form-group">
    	{{ Form::label('nama','Nama Pembeli',array('class'=>'')) }}
		{{ Form::text('nama', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
		<span style="color:red">{{ $errors->first('nama') }}</span>
	</div>
    <div class="form-group">
        {{ Form::label('alamat','Alamat',array('class'=>'')) }}
        {{ Form::text('alamat', null, array('class'=>'form-control','placeholder'=>'Alamat lengkap sesuai dengan alamat yang akan dikirimkan')) }}
        <span style="color:red">{{ $errors->first('alamat') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('kodePos','Kode Pos',array('class'=>'')) }}
        {{ Form::text('kodePos', null, array('class'=>'form-control','placeholder'=>'Kode pos')) }}
        <span style="color:red">{{ $errors->first('kodePos') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('jenisBarang','Jenis Barang Lengkap + Jumlah',array('class'=>'')) }}
        {{ Form::textarea('jenisBarang', null, array('class'=>'form-control','placeholder'=>'Case iPhone Black : 1 pieces')) }}
        <span style="color:red">{{ $errors->first('jenisBarang') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('totalBerat','Total Berat',array('class'=>'')) }}
        {{ Form::text('totalBerat', null, array('class'=>'form-control','placeholder'=>'Total berat')) }}
        <span style="color:red">{{ $errors->first('totalBerat') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('phoneAtauLine','No. Telp Pembeli',array('class'=>'')) }}
        {{ Form::text('phoneAtauLine', null, array('class'=>'form-control','placeholder'=>'No. telpon / LINE ID yang dapat dihubungi')) }}
        <span style="color:red">{{ $errors->first('phoneAtauLine') }}</span>
    </div>
    
    <div class="form-group">
        {{ Form::label('totalHarga','Total Harga Barang',array('class'=>'')) }}
        {{ Form::text('totalHarga', null, array('class'=>'form-control','placeholder'=>'Total harga barang yang dibeli')) }}
        <span style="color:red">{{ $errors->first('totalHarga') }}</span>
    </div>
    
    <div class="form-group">
        {{ Form::label('biayaOngkir','Biaya Ongkir',array('class'=>'')) }}
        {{ Form::text('biayaOngkir', null, array('class'=>'form-control','placeholder'=>'Biaya ongkir yang diberikan oleh seller')) }}
        <span style="color:red">{{ $errors->first('biayaOngkir') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('imageBuktiPembayaran','Screenshot Bukti Transfer',array('class'=>'')) }}
        {{ Form::file('imageBuktiPembayaran') }}
        <span style="color:red">{{ $errors->first('imageBuktiPembayaran') }}</span>
    </div>

        {{ Form::hidden('confirmed', 0, array()) }}
        @if(!Auth::check())
            {{ Form::submit('Order +', array('class'=>'btn btn-primary btn-lg btn-block','style'=>'background-color:#ff743d;border-color:white')) }}
        @else
            {{ Form::submit('Save', array('class'=>'btn btn-primary btn-lg btn-block','style'=>'background-color:#ff743d;border-color:white')) }}
        @endif
    	

    {{ Form::close() }}

</div>

<script>
jQuery(document).ready(function() {       
  OrderJS.init();
});
</script>

@stop
@extends('layouts.default')

@section('content')
<!-- <div class="container">
    {{ Form::open(array('route'=>'order.store','method'=>'post','role'=>'form')) }}
    <div class="form-group">
        {{ Form::label('email','Email',array('class'=>'')) }}
        {{ Form::email('email', null, array('class'=>'form-control','placeholder'=>'Email yang aktif')) }}
        <span style="color:red">{{ $errors->first('email') }}</span>
    </div>
    <div class="form-group">
    	{{ Form::label('nama','Nama Lengkap',array('class'=>'')) }}
		{{ Form::text('nama', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
		<span style="color:red">{{ $errors->first('nama') }}</span>
	</div>
    <div class="form-group">
        {{ Form::label('alamat','Alamat Lengkap',array('class'=>'')) }}
        {{ Form::text('alamat', null, array('class'=>'form-control','placeholder'=>'Alamat lengkap sesuai dengan alamat yang akan dikirimkan')) }}
        <span style="color:red">{{ $errors->first('alamat') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('jenisBarang','Jenis Barang',array('class'=>'')) }}
        {{ Form::textarea('jenisBarang', null, array('class'=>'form-control','placeholder'=>'Case iPhone Black : 1 pieces')) }}
        <span style="color:red">{{ $errors->first('jenisBarang') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('phoneAtauLine','Phone / LINE ID',array('class'=>'')) }}
        {{ Form::text('phoneAtauLine', null, array('class'=>'form-control','placeholder'=>'No. telpon / LINE ID yang dapat dihubungi')) }}
        <span style="color:red">{{ $errors->first('phoneAtauLine') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('imageBuktiPembayaran','Foto Bukti Pembayaran',array('class'=>'')) }}
        {{ Form::file('imageBuktiPembayaran') }}
        <span style="color:red">{{ $errors->first('imageBuktiPembayaran') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('totalHarga','Total Harga Barang',array('class'=>'')) }}
        {{ Form::text('totalHarga', null, array('class'=>'form-control','placeholder'=>'Total harga barang yang dibeli')) }}
        <span style="color:red">{{ $errors->first('totalHarga') }}</span>
    </div>
    <div class="form-group">
        {{ Form::label('biayaOngkir','Biaya Ongkir',array('class'=>'')) }}
        {{ Form::text('biayaOngkir', null, array('class'=>'form-control','placeholder'=>'Biaya ongkir yang diberikan oleh seller')) }}
        <span style="color:red">{{ $errors->first('biayaOngkir') }}</span>
    </div>
        {{ Form::hidden('confirmed', 0, array()) }}
    	{{ Form::submit('Order', array('class'=>'btn btn-primary btn-lg')) }}

    {{ Form::close() }}
</div> -->
@stop
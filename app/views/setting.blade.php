@extends('layouts.default')

@section('content')
<div class="container">
	<div class="row">
		<!-- <div class="col-sm-3">
			<div class="list-group">
			  <a href="#general" class="list-group-item" data-toggle="tab" role="tab" aria-controls="general">
			    General Information
			  </a>
			  <a href="#chg_password" class="list-group-item" data-toggle="tab" role="tab" aria-controls="chg_password">Change Password</a>
			</div>
			<hr>
		</div> -->
		<div class="col-sm-12">
			<!-- <div class="tab-content"> -->
				<div class="tab-pane fade in active" role="tabpanel" id="general">
					<h1>General Information</h1>
					{{ Form::model($user, array('route'=>array('put.setting.editUser',$user->id),'method'=>'put')) }}
					<div class="form-group">
					    {{ Form::label('email','Email Aktif',array('class'=>'')) }}
					    {{ Form::email('email', null, array('class'=>'form-control','placeholder'=>'Email yang aktif')) }}
					    <span style="color:red">{{ $errors->first('email') }}</span>
					</div>
					<div class="form-group">
					    {{ Form::label('online_shop_name','Online Shop Name',array('class'=>'')) }}
					    {{ Form::text('online_shop_name', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
					    <span style="color:red">{{ $errors->first('online_shop_name') }}</span>
					</div>
					<div class="form-group">
					    {{ Form::label('name','Nama Lengkap',array('class'=>'')) }}
					    {{ Form::text('name', null, array('class'=>'form-control','placeholder'=>'Nama sesuai dengan yang akan ada di bill')) }}
					    <span style="color:red">{{ $errors->first('name') }}</span>
					</div>
					<div class="form-group">
					    {{ Form::label('phoneAtauLine','No. Telp',array('class'=>'')) }}
					    {{ Form::text('phoneAtauLine', null, array('class'=>'form-control','placeholder'=>'No. telpon / LINE ID yang dapat dihubungi')) }}
					    <span style="color:red">{{ $errors->first('phoneAtauLine') }}</span>
					</div>
					<div class="form-group">
				        {{ Form::label('alamat','Alamat',array('class'=>'')) }}
				        {{ Form::text('alamat', null, array('class'=>'form-control','placeholder'=>'Alamat lengkap sesuai dengan alamat yang akan dikirimkan')) }}
				        <span style="color:red">{{ $errors->first('alamat') }}</span>
				    </div>
					{{ Form::submit('Update', array('class'=>'btn btn-primary btn-lg','style'=>'background-color:#ff743d;border-color:white')) }}
					{{ Form::close() }}
				</div>
				<hr>
				<div class="tab-pane fade in" role="tabpanel" id="chg_password">
					<h1>Password</h1>
					{{ Form::open(array('route'=>'put.setting.changePassword','method'=>'put','role'=>'form')) }}
					<div class="form-group">
				        {{ Form::label('current_password','Current Password',array('class'=>'')) }}
				        {{ Form::password('current_password', array('class'=>'form-control','placeholder'=>'Password')) }}

				        <span style="color:red">{{ $errors->first('password') }}</span>
				    </div>
					<div class="form-group">
				        {{ Form::label('new_password','New Password',array('class'=>'')) }}
				        {{ Form::password('new_password', array('class'=>'form-control','placeholder'=>'Password')) }}

				        <span style="color:red">{{ $errors->first('password') }}</span>
				    </div>

				    <div class="form-group">
				        {{ Form::label('password_again','Confirm New Password',array('class'=>'')) }}
				        {{ Form::password('password_again', array('class'=>'form-control','placeholder'=>'Re-Password again')) }}
				        <span style="color:red">{{ $errors->first('password_again') }}</span>
				    </div>
				    {{ Form::submit('Change Password', array('class'=>'btn btn-primary btn-lg','style'=>'background-color:#ff743d;border-color:white')) }}
					{{ Form::close() }}
				</div>
			<!-- </div> -->
		</div>
	</div>
</div>
@stop
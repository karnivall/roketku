<nav class="navbar navbar-default" role="navigation">
  <div>
    <img src="/uploads/roketku_logo.png" alt="Roketku" style="max-height:100px">
  </div>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="{{ URL::route('order.specific') }}">Isi Resi Online
        @if(!Auth::check())
          oleh Buyer
        @else
          oleh Seller
        @endif


      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?php
        $hostparts = explode('.',$_SERVER['HTTP_HOST']);
        $username = $hostparts[0];
        $userFromSubdomain = User::where('username','=',$username)->first();
        ?>

        @if($userFromSubdomain->role !== 'Admin')
          <li><a href="{{ URL::route('order.specific') }}"><strong>Order</strong></a></li>
        @endif

        @if(!Auth::check())
        	<li><a href="{{ URL::route('get.login.showLogin') }}"><strong>Login</strong></a></li>
        @else
          @if($userFromSubdomain->role === 'Admin')
            <li><a href="{{ URL::route('get.admin.showList') }}"><strong>List</strong></a></li>
            <li><a href="{{ URL::route('get.register.showRegister') }}"><strong>Register new user</strong></a></li>
          @else
            <li><a href="{{ URL::route('seller.order.listing') }}"><strong>Dashboard</strong></a></li>
          @endif
          <li><a href="{{ URL::route('get.setting.show') }}"><strong>Setting</strong></a></li>
          <li><a href="{{ URL::route('get.login.doLogout') }}"><strong>Logout</strong></a></li>
        @endif
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
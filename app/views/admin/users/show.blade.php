@extends('seller.default')

@section('content')
<div class="container">
    <div class = "row">
        <div class="col-md-12" style="text-align:center"><h1>List Users</h1></div>
    </div>

    @if(count($users))
    <div class ="row" style="margin-top:20px">
    	<div class ="table-responsive">
			<table class="table table-bordered table-striped table-responsive">
				<thead>
					<th></th>
					<th>Profile Picture</th>
					<th>Username</th>
					<th>Online Shop Name</th>
					<th>Email</th>
					<th>Alamat</th>
			        <th>Contact No.</th>
			        <th>Status</th>
				</thead>
				<tbody class="image-link">
					<?php $i = 0;?>
					@foreach($users as $user)
					<?php $i++  ;?>
						@if ($user->confirmed)
					    	<tr class='success'>
						@else
							<tr>
					    @endif
							    <td>{{ $i }}
						    	<td style="text-align:center">
				                	<a href="/{{{ $user->profile_picture }}}">
					                	<img src="/{{{ $user->profile_picture }}}" alt="..." class="img-circle" style="width:100px; height:100px;">
					                </a>
				                </td>
							    </td>
								<td>{{{ $user->username }}}</td>
				<!--				<td><img src="{{{ $user->imageBuktiPembayaran }}}"></td>-->
				                <td>{{{ $user->online_shop_name}}}</td>
								<td>{{{ $user->email }}}</td>
				                
				                <td>{{{ $user->alamat }}}</td>
				                <td>{{{ $user->phoneAtauLine }}}</td>
				                <td>{{{ $user->statusOnline }}}  </td>
								<td>
									{{ link_to_route('get.admin.editUser','Edit',array($user->id)) }}
							    </td>
				                
							</tr>
					@endforeach
				
				</tbody>
			</table>
        </div>
	</div>
	<!-- <div class = "row">
        <div class ="col-md-4 col-md-offset-6" style="font-size:30px;font-weight:bold;text-align:center;">Total Penjualan Hari Ini</div>
        <div class ="col-md-2" style="font-size:18px;margin-top:10px;text-align:center;">Rp. </div>
    </div> -->
    @else
		Belum ada user
	@endif
</div>

<script>
jQuery(document).ready(function() {
  //function here
  HomeJS.init();
});
</script>
@stop
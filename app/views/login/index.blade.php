@extends('layouts.default')

@section('content')
<div class="container">
	{{ Form::open(array('url' => 'admin/login')) }}
		<h1>Login</h1>

		<div class="form-group">
	        {{ Form::label('email','Email',array('class'=>'')) }}
	        {{ Form::email('email', Input::old('email'), array('class'=>'form-control','placeholder'=>'awesome@awesome.com')) }}
	        <span style="color:red">{{ $errors->first('email') }}</span>
	    </div>

	    <div class="form-group">
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password', array('class'=>'form-control')) }}
			<span style="color:red">{{ $errors->first('password') }}</span>
		</div>

		<p>{{ Form::submit('Login', array('class'=>'btn btn-primary btn-lg')) }}</p>
	{{ Form::close() }}
</div>
@stop
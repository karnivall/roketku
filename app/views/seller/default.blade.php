<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Seller Admin Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    {{ HTML::style('assets/stylesheets/backend.css') }}

    <script src="{{ URL::asset('assets/javascript/backend.js') }}"></script>
</head>
<body>

@include('partials.menu')

@yield('content')

</body>
</html>
@extends('seller.default')

@section('content')
<div class="container">
    <div class = "row">
        <div class="col-md-12" style="text-align:center"><img src="{{{$user->profile_picture}}}" class="img-circle" style="width:80px;height:80px">
        </div>
    </div>
    <div class = "row">
        <div class="col-md-12" style="text-align:center"><h1>{{ $user->online_shop_name }}</h1></div>
        <div class="col-md-12" style="text-align:center"><h1>Dashboard</h1></div>
    </div>

    @if(count($orders))
    <div class ="row" style="margin-top:20px">
    	<div class ="table-responsive">
			<table class="table table-bordered table-striped table-responsive">
				<thead>
					<th>Order</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>Jenis Barang   Jumlah</th>
					<th>Berat</th>
					<th>Bukti Transfer</th>
			        <th>Biaya Ongkir</th>
			        <th>Harga Barang</th>
				</thead>
				<tbody class="image-link">
					<?php $i = 0;?>
					@foreach($orders as $order)
					<?php $i++  ;?>
						@if ($order->confirmed)
					    	<tr class='success'>
						@else
							<tr>
					    @endif
							    <td>{{ $i }}
							    </td>
								<td>{{{ $order->nama }}}</td>
				<!--				<td><img src="{{{ $order->imageBuktiPembayaran }}}"></td>-->
				                <td>{{{ $order->alamat . '- Kode Pos: ' . $order->kodePos }}}</td>
								<td>{{{ $order->jenisBarang }}}</td>
								<td>{{{ $order->totalBerat . ' kg' }}}</td>
				                <td style="text-align:center">
				                	<a href="{{{ $order->imageBuktiPembayaran }}}">
					                	<img src="{{{ $order->imageBuktiPembayaran }}}" alt="..." class="img-rounded" style="width:100px; height:100px;">
					                </a>
				                </td>
				                <td>{{{ $order->biayaOngkir }}}</td>
				                <td>{{{ $order->totalHarga }}}</td>
								<td>
									@if ($order->confirmed)
									<a href="#">Print</a>
									@else
									<a href="#">Confirm</a>
									@endif
							    </td>
				                
							</tr>
					@endforeach
				
				</tbody>
			</table>
        </div>
	</div>
	<div class = "row">
        <div class ="col-md-4 col-md-offset-6" style="font-size:30px;font-weight:bold;text-align:center;">Total Penjualan Hari Ini</div>
        <div class ="col-md-2" style="font-size:18px;margin-top:10px;text-align:center;">Rp. 
        <?php $total=0 ?>
        @foreach ($totalPenjualanUserHariIni as $penjualan)
        	<?php $total += $penjualan->totalHarga ?>
        @endforeach
        <strong>{{{ $total }}}</strong>
        </div>
    </div>
    @else
		Belum ada penjualan
	@endif
</div>

<script>
jQuery(document).ready(function() {
  //function here
  HomeJS.init();
});
</script>
@stop